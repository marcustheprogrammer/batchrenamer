# README #

This is a batch re-namer written in bash. It was specifically written for work to find if the old name had any current PCN numbers in the name. If so it kept them and renamed the file with a new assigned name, kept the PCN's or said NO PCN if there weren't any found, and included a date.