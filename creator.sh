#pdftotext 2.pdf - | grep -P '^(7|D|R)[0-9]{5}$'

var1=1;
var2="";
name="";
date="";

echo "Assign a file name"
read varfile
echo $varfile

for f in *.pdf; 
do
	var2=`pdftotext "$f" - |  grep -P '^(7|D|R|8)[0-9]{5}' | awk '{print $1}' ORS=' '`

	name="${var2%.*}";	
	name=$(echo "${name}" | sed -e 's/^ *//' -e 's/ *$//' );

	date=`stat "$f" | grep "Change" | awk '{print $2}'`

	if [ -z "$var2" ]; then
		echo "$varfile $date NO PCNS ($var1).pdf" 
		mv "$f" "$varfile $date NO PCNS ($var1).pdf"
	else 
		echo "$varfile $date $name ($var1).pdf"
		mv "$f" "$varfile $date $name ($var1).pdf"
	fi

	var1=$((var1+1));

done
